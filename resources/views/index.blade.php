<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Digest Exam</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    

    <style>
        body { font: Montserrat; }
    </style>
</head>

<body>

    <header>

        <nav>
            <div class="logo">
                <h4 class=""><a href="#">Digest</a></h4>
            </div>

            <ul class="navLink">
                
                <li><a href="#">Home</a></li>
                <li><a href="#">Decisions</a></li>
                <li><a href="#">Laws</a></li>
                <li><a href="#">Resources</a></li>
                <li><a href="#">Lawyers</a></li>
                <li><a href="#">Contact Us</a></li>

            </ul>

            <div class="burger">
                
                <div class="line1"></div>
                <div class="line2"></div>
                <div class="line3"></div>

            </div>

        </nav>

    </header>

    <div class="container mt-5">
     <div class="toggle mt-5"></div>  
      <div class="jumbotron">
        <div class="row">
            <div class="col-md-10">
                <h2><b>Digest of:</b>
                    <h5 class="text-secondary mt-3"> {{$digest->title}} </h5>
                </h2>
            </div>

            <div class="col-md-2">
                <h6><b>Author:<span class="text-secondary"> {{$digest->author}} </span></b></h6>
                <h6><b>Subject:<span class="text-secondary"> {{$digest->category}} </span></b></h6>
                <h6><b>Rating:<span class="text-secondary"> {{$digest->rating}} </span></b></h6>
            </div>

            <div class="col-md-9 mt-4">
                <hr>
                <h5 class="mt-5"><b>Summary:</b>
                    <h6 class="text-secondary mb-5"> {{$digest->summary}} </h6>
                </h5>


                <h5 class="text-dark mt-5"> <b>Doctrine:</b>
                    <h6 class="text-secondary">{{$digest->summary}}</h6>
                </h5>

                <h5 class="text-dark mt-5"><b>Facts:</b>
                    <h6 class="text-secondary"> {{$digest->facts}} </h6>
                </h5>

                <h5 class="text-dark mt-5"><b>Issues_ratio:</b>
                    <h6 class="text-secondary"> {{$digest->issues_ratio}} </h6>
                </h5>

                <h5 class="text-dark mt-5"><b>Dispositive:</b>
                    <h6 class="text-secondary"> {{$digest->dispositive}} </h6>
                </h5>

                <h5 class="text-dark mt-5"><b>Other Notes:</b>
                    <h6 class="text-secondary"> {{$digest->note}} </h6>
                </h5>

            </div>
        </div>
      </div>

    </div>

    <script src="{{ asset('js/app.js') }}"></script>  
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.toggle').click(function(){
                $('.toggle').toggleClass('active')
                $('body').toggleClass('night')
            })
        })
    </script>

</body>
</html>
