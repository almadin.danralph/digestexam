<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DigestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('App\Digest');
        $arrayValues = ['civil', 'criminal', 'taxation','commercial','labor','political','remedial','ethics'];
        $rateValues = ['0', '1', '2','3','4','5'];

    	
        DB::table('digests')->insert([
            'author' => $faker->text(8), 
            'title' => $faker->text(50),
            'category' => $arrayValues[rand(0,7)],
            'summary' => $faker->text(500),
            'doctrine' => $faker->text(500),
            'facts' => $faker->text(500),
            'issues_ratio' => $faker->text(500),
            'dispositive' => $faker->text(500),
            'note' => $faker->text(200),	            
            'rating' => $rateValues[rand(0,5)],
        ]);
	    
    }
}
